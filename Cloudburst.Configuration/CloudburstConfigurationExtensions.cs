using System;
using System.Net;
using System.Reflection;
using System.Text;
using Cloudburst.Configuration.Attributes;
using Cloudburst.Configuration.Models;
using Cloudburst.Version.Abstractions;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Winton.Extensions.Configuration.Consul;

namespace Cloudburst.Configuration {
	public static class CloudburstConfigurationExtensions {

		public static IHostBuilder UseCloudburstConfiguration(this IHostBuilder builder) {
			return builder.UseCloudburstConfiguration(null, null);
		}

		public static IHostBuilder UseCloudburstConfiguration<TServiceVersion>(this IHostBuilder builder) where TServiceVersion : class, IServiceVersion {
			return builder.UseCloudburstConfiguration(typeof(TServiceVersion), null);
		}

		public static IHostBuilder UseCloudburstConfiguration<TServiceVersion>(this IHostBuilder builder, Action<IConsulConfigurationSource> consulConfigurationAction) where TServiceVersion : class, IServiceVersion {
			return builder.UseCloudburstConfiguration(typeof(TServiceVersion), consulConfigurationAction);
		}

		public static IHostBuilder UseCloudburstConfiguration(this IHostBuilder builder, Type versionType, Action<IConsulConfigurationSource> consulConfigurationAction) {
			if (!(versionType is null) && !typeof(IServiceVersion).IsAssignableFrom(versionType)) {
				throw new ArgumentOutOfRangeException($"{nameof(versionType)} must inherit from IServiceVersion");
			}

			builder.ConfigureAppConfiguration((builderContext, config) => {
				IHostEnvironment env = builderContext.HostingEnvironment;

				config
					.AddJsonFile("appsettings.Local.json", true, true)

					.AddJsonFile("AppSettings.json", true, true)
					.AddJsonFile("AppSettings.{env.EnvironmentName}.json", true, true)
					.AddJsonFile("AppSettings.Local.json", true, true)

					.AddJsonFile("Settings.json", true, true)
					.AddJsonFile("Settings.{env.EnvironmentName}.json", true, true)
					.AddJsonFile("Settings.Local.json", true, true)

					.AddEnvironmentVariables();
			});

			builder.ConfigureAppConfiguration((builderContext, config) => {
				IHostEnvironment env = builderContext.HostingEnvironment;

				var currentConfig = config.Build();
				var configurationOptions = currentConfig.GetWithKey<ConfigurationOptions>();

				if (!configurationOptions.UseConsul) {
					return;
				}

				var keyStringBuilder = new StringBuilder();
				keyStringBuilder.Append(configurationOptions.ConsulOptions.RootKey);

				if (configurationOptions.ConsulOptions.UseVersioning) {
					if (versionType is null) {
						throw new Exception($"Configuration.ConsulOptions.UseVersioning was true, but no IServiceVersion class was specified.");
					}

					IServiceVersion serviceVersion = (IServiceVersion) Activator.CreateInstance(versionType);
					if (serviceVersion is null) {
						throw new Exception("No instance of IServiceVersion found. Could not add versioning to ConsulConfiguration Provider.");
					}

					keyStringBuilder.Append($"/{serviceVersion}");
				}

				if (configurationOptions.ConsulOptions.UseEnvironment) {
					keyStringBuilder.Append($"/{env.EnvironmentName}");
				}

				Console.WriteLine($"Configuration Consul Path is {keyStringBuilder}");

				// ConsulConfigurationSource is an internal class... WTF?
				// So now we need to duplicate the .AddConsul configuration... sigh.
				config
					.AddConsul($"{configurationOptions.ConsulOptions.RootKey}/Common", options => {
						options.ConsulConfigurationOptions = consulClientConfig => {
							consulClientConfig.Address = new Uri(configurationOptions.ConsulOptions.ConsulHost);
							consulClientConfig.Datacenter = configurationOptions.ConsulOptions.ConsulDataCenter;
						};

						options.Optional = true;
						options.ReloadOnChange = true;
						options.PollWaitTime = TimeSpan.FromSeconds(configurationOptions.ConsulOptions.PollWaitTime);
						options.OnLoadException = exceptionContext => exceptionContext.Ignore = false;

						consulConfigurationAction?.Invoke(options);
					})

					.AddConsul($"{keyStringBuilder}/AppSettings", options => {
						options.ConsulConfigurationOptions = consulClientConfig => {
							consulClientConfig.Address = new Uri(configurationOptions.ConsulOptions.ConsulHost);
							consulClientConfig.Datacenter = configurationOptions.ConsulOptions.ConsulDataCenter;
						};

						options.Optional = true;
						options.ReloadOnChange = true;
						options.PollWaitTime = TimeSpan.FromSeconds(configurationOptions.ConsulOptions.PollWaitTime);
						options.OnLoadException = exceptionContext => exceptionContext.Ignore = false;

						consulConfigurationAction?.Invoke(options);
					})

					.AddConsul(keyStringBuilder.ToString(), options => {
						options.ConsulConfigurationOptions = consulClientConfig => {
							consulClientConfig.Address = new Uri(configurationOptions.ConsulOptions.ConsulHost);
							consulClientConfig.Datacenter = configurationOptions.ConsulOptions.ConsulDataCenter;
						};

						options.Optional = true;
						options.ReloadOnChange = true;
						options.PollWaitTime = TimeSpan.FromSeconds(configurationOptions.ConsulOptions.PollWaitTime);
						options.OnLoadException = exceptionContext => exceptionContext.Ignore = false;

						consulConfigurationAction?.Invoke(options);
					});

				CheckConsulKeys(
					configurationOptions.ConsulOptions.ConsulHost,
					configurationOptions.ConsulOptions.ConsulDataCenter,
					new [] {
						$"{configurationOptions.ConsulOptions.RootKey}/Common",
						$"{keyStringBuilder}/AppSettings"
					});

			});

			builder.ConfigureServices((builderContext, services) => {
				services.ConfigureMonitor<ConfigurationOptions>(builderContext.Configuration);
			});

			return builder;
		}

		private static void CheckConsulKeys(string consulHost, string dataCenter, string[] keys) {
			var consulClient = new ConsulClient(consulConfig => {
				consulConfig.Address = new Uri(consulHost);
				consulConfig.Datacenter = dataCenter;
			});

			foreach (string key in keys) {
				var value = consulClient.KV.Get(key).Result;
				if (value.StatusCode == HttpStatusCode.NotFound) {
					consulClient.KV.Put(
						new KVPair(key) {
							Flags = 42,
							Value = Encoding.UTF8.GetBytes("{}")
						});
				}
			}
		}

		public static T GetWithKey<T>(this IConfiguration configuration) {
			string configurationKey = GetConfigurationKey<T>();
			return configuration.GetSection(configurationKey).Get<T>();
		}

		public static IOptionsMonitor<T> ConfigureMonitor<T>(this IServiceCollection services, IConfiguration configuration) where T : class {
			string configurationKey = GetConfigurationKey<T>();

			services.AddOptions<T>()
				.Bind(configuration.GetSection(configurationKey))
				.ValidateDataAnnotations();

			return services.BuildServiceProvider().GetService<IOptionsMonitor<T>>();
		}

		private static string GetConfigurationKey<T>() {
			ConfigurationKeyAttribute configurationKey = typeof(T).GetCustomAttribute<ConfigurationKeyAttribute>();

			if (configurationKey is null) {
				throw new ArgumentNullException($"{typeof(T)} is missing required attribute [ConfigurationKey(\"\")]");
			}

			if (String.IsNullOrEmpty(configurationKey.Key)) {
				throw new ArgumentNullException($"[ConfigurationKey()] on {typeof(T)} cannot be null or empty.");
			}

			return configurationKey.Key;
		}

	}
}
